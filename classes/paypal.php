<?php
class PayPal {
	protected $_errors = array();

	/**
	* Данные API
	* @var array
	*/
	protected $_credentials = array(
	  'USER' => 'info-facilitator_api1.creditblaustein.com',
	  'PWD' => 'VCBPLHEW9XCWY7DM',
	  'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AYGfzJ.8ExHV3jI4pSp6l3oPSo4q',
	);

	/**
	* Указываем, куда будет отправляться запрос
	* Реальные условия - https://api-3t.paypal.com/nvp
	* Песочница - https://api-3t.sandbox.paypal.com/nvp
	* @var string
	*/
	protected $_endPoint = 'https://api-3t.sandbox.paypal.com/nvp';

	/**
	* Версия API
	* @var string
	*/
	protected $_version = '74.0';

	/**
	* Сформировываем запрос
	*
	* @param string $method Данные о вызываемом методе перевода
	* @param array $params Дополнительные параметры
	* @return array / boolean Response array / boolean false on failure
	*/
	public function request($method,$params = array()) {
	  $this -> _errors = array();
	  if( empty($method) ) { // Проверяем, указан ли способ платежа
	     $this -> _errors = array('Не указан метод перевода средств');
	     return false;
	  }

	  // Параметры нашего запроса
	  $requestParams = array(
	     'METHOD' => $method,
	     'VERSION' => $this -> _version
	  ) + $this -> _credentials;

	  // Сформировываем данные для NVP
	  $request = http_build_query($requestParams + $params);

	  // Настраиваем cURL
	  $curlOptions = array (
	     CURLOPT_URL => $this -> _endPoint,
	     CURLOPT_VERBOSE => 1,
	     CURLOPT_SSL_VERIFYPEER => true,
	     CURLOPT_SSL_VERIFYHOST => 2,
	     CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem', // Файл сертификата
	     CURLOPT_RETURNTRANSFER => 1,
	     CURLOPT_POST => 1,
	     CURLOPT_POSTFIELDS => $request
	  );

	  $ch = curl_init();
	  curl_setopt_array($ch,$curlOptions);

	  // Отправляем наш запрос, $response будет содержать ответ от API
	  $response = curl_exec($ch);

	  // Проверяем, нету ли ошибок в инициализации cURL
	  if (curl_errno($ch)) {
	     $this -> _errors = curl_error($ch);
	     curl_close($ch);
	     return false;
	  } else  {
	     curl_close($ch);
	     $responseArray = array();
	     parse_str($response,$responseArray); // Разбиваем данные, полученные от NVP в массив
	     return $responseArray;
	  }
	}

	public function newPayment($summ, $currency, $description, $name=null){
		// $requestParams = array(
		//    'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
		//    'PAYMENTACTION' => 'Sale'
		// );

		// $creditCardDetails = array(
		//    'CREDITCARDTYPE' => 'Visa',
		//    'ACCT' => '4929802607281663',
		//    'EXPDATE' => '062019',
		//    'CVV2' => '984'
		// );

		// $payerDetails = array(
		//    'FIRSTNAME' => (isset($name)) ? $name : 'John',
		//    'LASTNAME' => 'Doe',
		//    'COUNTRYCODE' => 'US',
		//    'STATE' => 'NY',
		//    'CITY' => 'New York',
		//    'STREET' => '14 Argyle Rd.',
		//    'ZIP' => '10010'
		// );

		// $orderParams = array(
		//    'AMT' => $summ,
		//    'ITEMAMT' => $summ,
		//    'SHIPPINGAMT' => '0',
		//    'CURRENCYCODE' => $currency
		// );

		// $item = array(
		//    'L_NAME0' => 'iPhone',
		//    'L_DESC0' => $description,
		//    'L_AMT0' => $summ,
		//    'L_QTY0' => '1'
		// );

		// $response = $this->request('DoDirectPayment',
		//    $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item
		// );

		$requestParams = array(
		   'RETURNURL' => 'http://webart-lviv.com.ua/work/leblavcode/success.php',
		   'CANCELURL' => 'http://webart-lviv.com.ua/work/leblavcode/cancelled.php'
		);

		$orderParams = array(
		   'PAYMENTREQUEST_0_AMT' => $summ,
		   'PAYMENTREQUEST_0_SHIPPINGAMT' => '0',
		   'PAYMENTREQUEST_0_CURRENCYCODE' => $currency,
		   'PAYMENTREQUEST_0_ITEMAMT' => $summ
		);

		$item = array(
		   'L_PAYMENTREQUEST_0_NAME0' => 'iPhone',
		   'L_PAYMENTREQUEST_0_DESC0' => $description,
		   'L_PAYMENTREQUEST_0_AMT0' => $summ,
		   'L_PAYMENTREQUEST_0_QTY0' => '1'
		);

		$response = $this -> request('SetExpressCheckout',$requestParams + $orderParams + $item);

		if( is_array($response) ) { 
			if($response['ACK'] == 'Success'){
				$token = $response['TOKEN'];
      			header( 'Location: https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token) );
			} else if($response['ACK'] == 'SuccessWithWarning'){
				$add = (isset($response['L_LONGMESSAGE0'])) ? "( код: {$response['L_ERRORCODE0']} | {$response['L_LONGMESSAGE0']} )" : null ;
				$message = "Операция была проведена успешно, но не идеально.{$add}";
			} else if($response['ACK'] == 'Failure'){
				$add = (isset($response['L_LONGMESSAGE0'])) ? "( код: {$response['L_ERRORCODE0']} | {$response['L_LONGMESSAGE0']} )" : null ;
				$message = "Операция не была успешной.{$add}";
			} else if($response['ACK'] == 'FailureWithWarning'){
				$add = (isset($response['L_LONGMESSAGE0'])) ? "( код: {$response['L_ERRORCODE0']} | {$response['L_LONGMESSAGE0']} )" : null ;
				$message = "Операция не была успешной 2.{$add}";
			}else{
				$message = "ОШИБКА! Ответ пришел но его не получилось розпознать!";
			}
		}else{
				$message = $response."ОШИБКА! Ответ не получен!";
			}
		if(is_array($this -> _errors)){
			foreach ($this -> _errors as $key => $error) {
				$message .= " ERROR_{$key}: {$error}.";
			}
		}else{
			$message .= " ERROR: " . $this -> _errors;
		}
		return $message;
	}
}