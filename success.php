<?php

include_once "classes/paypal.php";
if( isset($_GET['token']) && !empty($_GET['token']) ) { // Токен присутствует
   // Получаем детали оплаты, включая информацию о покупателе.
   // Эти данные могут пригодиться в будущем для создания, к примеру, базы постоянных покупателей
   $paypal = new PayPal();
   $checkoutDetails = $paypal -> request('GetExpressCheckoutDetails', array('TOKEN' => $_GET['token']));

   // Завершаем транзакцию
   $requestParams = array(
   	  'TOKEN' => $_GET['token'],
      'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
      'PAYERID' => $_GET['PayerID'],
      'L_BILLINGTYPE0' => 'RecurringPayments',
      'L_BILLINGAGREEMENTDESCRIPTION0' => 'SamePayments',
      'PAYMENTREQUEST_0_AMT' => $checkoutDetails['PAYMENTREQUEST_0_AMT'],
      'PAYMENTREQUEST_0_CURRENCYCODE' => $checkoutDetails['PAYMENTREQUEST_0_CURRENCYCODE']
   );

   $response = $paypal -> request('DoExpressCheckoutPayment',$requestParams);
   if( is_array($response) ) {
   		if($response['ACK'] == 'Success') { // Оплата успешно проведена
	      // Здесь мы сохраняем ID транзакции, может пригодиться во внутреннем учете
	     $transactionId = $response['PAYMENTINFO_0_TRANSACTIONID'];
	      $message = "транзакция успешна! Код транзакции: {$transactionId} для пользователя {$_GET['PayerID']}";
	    } else if($response['ACK'] == 'SuccessWithWarning'){
			$add = (isset($response['L_LONGMESSAGE0'])) ? "( код: {$response['L_ERRORCODE0']} | {$response['L_LONGMESSAGE0']} )" : null ;
			$message = "Операция была проведена успешно, но не идеально.{$add}";
		} else if($response['ACK'] == 'Failure'){
			$add = (isset($response['L_LONGMESSAGE0'])) ? "( код: {$response['L_ERRORCODE0']} | {$response['L_LONGMESSAGE0']} )" : null ;
			$message = "Операция не была успешной.{$add}";
		} else if($response['ACK'] == 'FailureWithWarning'){
			$add = (isset($response['L_LONGMESSAGE0'])) ? "( код: {$response['L_ERRORCODE0']} | {$response['L_LONGMESSAGE0']} )" : null ;
			$message = "Операция не была успешной 2.{$add}";
		}else{
			$message = "ОШИБКА! Ответ пришел но его не получилось розпознать!";
		}
    }else{
			$message = $response."ОШИБКА! Ответ не получен!";
	}
	echo $message;
}