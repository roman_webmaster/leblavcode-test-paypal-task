<?php
if(isset($_POST['new_payment'])){
	include_once "classes/paypal.php";
	$name = (isset($_POST['name'])) ? $_POST['name'] : null ;
	$summ = (isset($_POST['summ'])) ? $_POST['summ'] : null ;
	$currency = (isset($_POST['currency'])) ? $_POST['currency'] : null ;
	$description = (isset($_POST['description'])) ? $_POST['description'] : null ;
	if(!is_null($summ) && !is_null($currency) && !is_null($description)){
		$request = new PayPal();
		echo $request->newPayment($summ, $currency, $description, $name);
	}else{
		echo "Ошибка! обратитесь к администратору";
	}
}