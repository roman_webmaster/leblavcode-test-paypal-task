<!DOCTYPE html>
<html>
<head>
	<title>Тестове завдання leblavcode</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	<!-- Latest compiled and minified JavaScript -->
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<section class="general container-fluid">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="logo">
				<img src="img/logo.jpg" alt="">
			</div>
			<form role="form" method="POST" action="pay.php">
			  <div class="form-group">
			    <label for="inputName">Имя</label>
			    <input type="text" name="name" class="form-control" id="inputName" placeholder="Ваше имя">
			  </div>
			  <div class="form-group">
			    <label for="inputSumm">Cумма <span style="color:red;">*</span></label>
			    <input type="number" name="summ" class="form-control" id="inputSumm" placeholder="Введите сумму платежа" required="required">
			  </div>
			  <div class="form-group">
			    <label for="inputCurrency">Валюта <span style="color:red;">*</span></label>
			    <select class="form-control" name="currency" id="inputCurrency">
			      <option selected="selected" disabled="disabled">Виберите валюту</option>
				  <option value="UAH">Украинская гривна</option>
				  <option value="USD">Долар США</option>
				  <option value="EUR">Евро</option>
				  <option value="RUB">Российский рубль</option>
				</select>
			  </div>
			  <div class="form-group">
			    <label for="inputDescription">Описание платежа <span style="color:red;">*</span></label>
			    <textarea class="form-control" name="description" id="inputDescription" rows="3" placeholder="Введите описание платежа" required="required"></textarea>
			  </div>
			  <div class="submit">
			  	<button type="button" class="btn form_input btn-success" name="new_payment">Отправить</button>
			  </div>
			</form>
		</div>
		<div class="col-md-3"></div>
	</section>
<script type="text/javascript">
$(".form_input").click(function() {
	var inputSumm = $(this).closest('form').find("#inputSumm").val(),
		inputCurrency = $(this).closest('form').find("#inputCurrency").val(),
		inputDescription = $(this).closest('form').find("#inputDescription").val(),
		inputName = $(this).closest('form').find("#inputName").val();

    if(inputSumm=="" || inputCurrency.length<2 || inputDescription=="") {
        alert('Заполните все поля!');
    }
});

$(".form_input").mouseover(function() {
	var inputSumm = $(this).closest('form').find("#inputSumm").val(),
		inputCurrency = $(this).closest('form').find("#inputCurrency").val(),
		inputDescription = $(this).closest('form').find("#inputDescription").val(),
		inputName = $(this).closest('form').find("#inputName").val();

    if(inputSumm!="" && inputCurrency.length>2 && inputDescription!=""){
	   $(this).removeAttr('type');
	   $(this).attr('type','submit');
    }
});
</script>
</body>
</html>